import React from 'react';
import './app.css';
import MessageList from "./Message/MessageList";

function App() {

  return (
    <div className="App">
      <MessageList />
    </div>
  );
}

export default App;
