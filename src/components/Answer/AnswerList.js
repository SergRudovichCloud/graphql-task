import React, { useState } from "react";
import AnswerItem from './AnswerItem';
import AnswerForm from './AnswerForm';

const AnswerList = props => {
    const [showAnswerForm, toggleAnswer] = useState(false);
    const { messageId, answers } = props;

    return (
        <div className="answer-list">
            {answers.length > 0 && <span className="answer-list-title">Answers</span>}
            {answers.map(item => {
                return <AnswerItem key={item.id} {...item} />
            })}
            <button className="answer-button" onClick={() => toggleAnswer(!showAnswerForm)}>
                {showAnswerForm ? 'Close' : 'Answer'}
            </button>
            {showAnswerForm && <AnswerForm
                messageId={messageId}
                toggleForm={toggleAnswer}
            />}
        </div>
    );
}

export default AnswerList;