function postMessage(parent, args, context, info) {
    return context.pr.createMessage({
        text: args.text,
        like: 0,
        dislike: 0
    })
}

async function postAnswer(parent, args, context, info) {
    const messageExists = await context.pr.$exists.message({
        id: args.messageId
    })

    if (!messageExists) {
        throw new Error(`Message id${args.messageId} dos not exists`)
    }

    return context.pr.createAnswer({
        text: args.text,
        like: 0,
        dislike: 0,
        message: {connect: {id: args.messageId}}
    })
}

module.exports = {
    postMessage,
    postAnswer
}