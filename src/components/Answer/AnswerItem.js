import React from "react";
import UserReaction from '../UserReaction';

const AnswerItem = props => {
    const { text, like, dislike } = props;

    return (
        <div>
            <p>{text}</p>
            <UserReaction
                like={like}
                dislike={dislike}
            />
        </div>

    );
}

export default AnswerItem;