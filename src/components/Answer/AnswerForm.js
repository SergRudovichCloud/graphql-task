import React, { useState } from "react";
import { useMutation } from "@apollo/client";
import { POST_ANSWER_MUTATION } from "../../queries";

const AnswerForm = props => {
    const {messageId} = props;
    const [text, setText] = useState('');
    const [sendAnswer, { data }] = useMutation(POST_ANSWER_MUTATION);
    return (
        <div className="form-wrapper">
            <div className="input-wrapper">
                <textarea
                    onChange={e => setText(e.target.value)}
                    placeholder="Type your answer"
                    autoFocus
                    value={text}
                    cols="25"
                />
                <button onClick={() => {
                    sendAnswer({ variables: { messageId: messageId, text: text } });
                    setText('');
                }}>Answer</button>
            </div>
        </div>
    )
}

export default AnswerForm;