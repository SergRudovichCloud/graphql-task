import React from 'react';
import AnswerList from '../Answer/AnswerList';
import UserReaction from '../UserReaction';
import "../app.css";

const MessageItem = props => {
    const { id, text, like, dislike, answers } = props;
    return (
        <div className="message-item">
            <div className="title-wrapper">
                <span>{id}</span>
                <h2>{text}</h2>
                <UserReaction 
                   like={like}
                   dislike={dislike}
                />
            </div>
            <AnswerList messageId={id} answers={answers} />
        </div>
    );
}

export default MessageItem;