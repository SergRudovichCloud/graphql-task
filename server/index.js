const { GraphQLServer } = require('graphql-yoga');
const prisma = require('../server/generated/prisma-client');
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const Message = require('./resolvers/Message');
const Answer = require('./resolvers/Answer');
const Subscription = require('./resolvers/Subscription');

const pr = prisma.prisma;

const resolvers = {
  Query,
  Mutation,
  Subscription,
  Answer,
  Message
}

const server = new GraphQLServer({
  typeDefs: './server/schema.graphql',
  resolvers,
  context: { pr }
});

server.start(() => {
  console.log(
    `Server running at http://localhost:4000`
  );
});