
import React from 'react';
import { useQuery } from "@apollo/client";
import { GET_ALL_MASSAGES } from "../../queries";
import MessageItem from "./MessageItem";
import MessageForm from "./MessageForm";

const MessageList = () => {
    const { loading, error, data } = useQuery(GET_ALL_MASSAGES);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    return (
        <div>
            {data.messages.messageList.map((item) => (
                <MessageItem key={item.id} {...item} />
            ))}
            <MessageForm />
        </div>
    )

}

export default MessageList;