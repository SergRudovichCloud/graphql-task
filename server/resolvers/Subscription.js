function newMassegeSubscribe(parent, args, context, info) {
    return context.pr.$subscribe.message({
        mutation_in: ['CREATED']
    }).node()
}

const newMessage = {
    subscribe: newMassegeSubscribe,
    resolve: payload => {
        return payload
    }
}

module.exports = {
    newMessage
}