import { gql } from '@apollo/client';

export const GET_ALL_MASSAGES = gql`
query{
  messages(orderBy: createdAt_DESC) {
    messageList{
        id text like dislike answers {
             id text like dislike
             } 
     }
    count
  }
}`;

export const POST_MESSAGE_MUTATION = gql`
  mutation MessageMutation($text: String!) {
    postMessage(text: $text){
        id
	text
      }
    }
`;

export const POST_ANSWER_MUTATION = gql`
  mutation AnswerMutation($messageId: String!, $text: String!) {
    postAnswer(messageId: $messageId, text: $text) {
        id
      text
    }
  }
`;