function answers(parent, args, context) {
    return context.pr.message({
        id: parent.id
    }).answers();
}

module.exports = {
    answers
}