function message(parent, args, context) {
    return context.pr.answer({
        id: parent.id
    }).message();
}

module.exports = {
    message
}