import React, { useState } from "react";
import { useMutation, gql } from "@apollo/client";
import { POST_MESSAGE_MUTATION } from "../../queries";

const MessageForm = props => {
    const [text, setText] = useState('');

    const [sendMessage] = useMutation(POST_MESSAGE_MUTATION, {
        update(cache, { data: { sendMessage } }) {
            cache.modify({
                fields: {
                    messageList(existingMessages = []) {
                        const newMessageRef = cache.writeFragment({
                            data: sendMessage,
                            fragment: gql`
                    fragment NewMessage on Message {
                      id
                      text
                    }
                  `
                        });
                        return [...existingMessages, newMessageRef];
                    }
                }
            });
        }
    });

    
    return (
        <div className="form-wrapper">
            <div className="input-wrapper">
                <textarea
                    onChange={e => setText(e.target.value)}
                    placeholder="Type your message"
                    autoFocus
                    value={text}
                />
            </div>

            <button className="message-button" onClick={() => {
                sendMessage({ variables: { text: text } });
                setText('');
            }}>Send</button>

        </div>
    )
}

export default MessageForm;