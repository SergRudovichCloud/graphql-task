import React from "react";

const UserReaction = props => {
    const {like, dislike} = props
    return (
        <div className="reaction-wrapper">
        <span>{like}</span><button>Like</button>
        <span>{dislike}</span><button>dislike</button>
    </div>
    )
}

export default UserReaction;